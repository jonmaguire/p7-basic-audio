/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::setGain (float newGain) //new gain is value from slider
{
    gainControl = newGain;
}

void Audio::setFrequency(float newFreq)
{
    
    frequency = newFreq;
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{

    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency.get())/sampleRate;
    
    while(numSamples--)
    {
        phasePosition += phaseIncrement;

        if (phasePosition > twoPi)
        phasePosition = phasePosition - twoPi;
            

        float sample = sin(phasePosition) * gainControl.get();
        
        *outL = sample;
        *outR = sample;
        
        inL++;
        inR++;
        outL++;
        outR++;
        
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{

    frequency = 0.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
    gainControl = 0.f;
    
}

void Audio::audioDeviceStopped()
{

}
