/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainComponent::MainComponent (Audio& a) : Thread ("CounterThread"), audio (a)
{
    startThread();
    
    gainControl.setRange (0.0, 0.25);
    gainControl.setTextBoxStyle (Slider::TextBoxRight, false, 100, 20);
    gainControl.addListener (this);
    gainLabel.setText ("Gain", dontSendNotification);
    
    freqControl.setRange (0.0, 15000.0);
    freqControl.setTextBoxStyle(Slider::TextBoxRight, false, 100, 20);
    
    
    freqControl.addListener (this);
    freqLabel.setText ("Frequency", dontSendNotification);
    
    addAndMakeVisible (gainControl);
    addAndMakeVisible (gainLabel);
    addAndMakeVisible (freqControl);
    addAndMakeVisible (freqLabel);
    
    
    
    setSize (500, 400);
}

MainComponent::~MainComponent()
{
    stopThread(500);
}

void MainComponent::resized() //size and placement on UI
{
    gainLabel.setBounds (10, 0, 100, 20); //pos from left, pos from top, width, height
    gainControl.setBounds (100, 0, getWidth() - 110, 30);
    
    freqLabel.setBounds (10, 40, 100, 20);
    freqControl.setBounds (100, 40, getWidth() - 110, 30);
    
   
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    if( slider == &gainControl) audio.setGain (slider->getValue());
    if( slider == &freqControl) audio.setFrequency(slider->getValue());
}

void MainComponent::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        std::cout << "Counter: " << Time::getApproximateMillisecondCounter() << std::endl;
        Time::waitForMillisecondCounter(time + 100);
    }
}



